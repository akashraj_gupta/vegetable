package com.app.android.vegetable;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;


import com.app.android.vegetable.utils.AppConstants;
import com.app.android.vegetable.utils.Logger;
import com.app.android.vegetable.utils.Utils;
import com.app.android.vegetable.widget.AnimatedExpandableListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


/**
 * Created by atarmanipandey on 29/3/16.
 */

public class NavigationActivity extends AppCompatActivity implements DrawerLayout.DrawerListener {

    private static final String TAG = "NavigationActivity.java";
    private static final int PERMISSIONS_REQUEST_CODE = 100;
    static int[] icon = {R.drawable.ic_home, R.drawable.ic_redigo, R.drawable.ic_redigo,
            R.drawable.ic_book_now, R.drawable.ic_history, R.drawable.ic_keep_me_posted, R.drawable.ic_dealer_locator,
            R.drawable.ic_buzz, R.drawable.ic_brand,
            R.drawable.ic_share_nav, R.drawable.ic_contact};

    public FrameLayout frameLayout;
    public ImageView ivNavigation;
    public DrawerLayout drawerLayout;
    public NavigationView navigationView;
    View view_Group;
    private Toolbar toolbar;
    private TextView tvTitle;
    private String screenName = "";
    private AnimatedExpandableListView expListView;
    private HashMap<String, List<String>> listDataChild;
    private ExpandableListAdapter listAdapter;
    private List<String> listDataHeader;
    private String permissionBeingAsked;
    private int[] productSummary = {R.drawable.slide1, R.drawable.slide2
            , R.drawable.slide3, R.drawable.slide4,
            R.drawable.slide5, R.drawable.slide6,
            R.drawable.slide7, R.drawable.slide8,
            R.drawable.slide9};
    private int[] compition = {R.drawable.slide1_compition, R.drawable.slide2_compition
            , R.drawable.slide3_compition, R.drawable.slide4_compition
    };
    private int[] testDrive = {R.drawable.slide1_testdrive};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.animateActivity(NavigationActivity.this, "next");
        setContentView(R.layout.layout_main_activity);
        frameLayout = (FrameLayout) findViewById(R.id.container);
        tvTitle = (TextView) findViewById(R.id.tvTitleText);
        ivNavigation = (ImageView) findViewById(R.id.ivNavigation);
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);


        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawerLayout.isDrawerOpen(GravityCompat.START))
                    drawerLayout.closeDrawers();
                else
                    drawerLayout.openDrawer(GravityCompat.START);
            }
        };

        ivNavigation.setOnClickListener(onClickListener);

        //to change width
        navigationView = (NavigationView) findViewById(R.id.navigation_view);
        int width = getResources().getDisplayMetrics().widthPixels / 2;


        drawerLayout.setDrawerListener(this);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            // Do something for lollipop and above versions
            drawerLayout.setFitsSystemWindows(true);
        } else {
            //navigationParent.setFitsSystemWindows(true);
        }

        setUpExpandableList();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void setUpExpandableList() {

        expListView = (AnimatedExpandableListView) findViewById(R.id.list_slidermenu);
        expListView.setChildDivider(null);
        expListView.setDivider(null);
        expListView.setChildIndicator(null);
        expListView.setGroupIndicator(null);

        prepareListData();


        listAdapter = new ExpandableListAdapter(this, listDataHeader,
                listDataChild, icon);

        expListView.setAdapter(listAdapter);

        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                Logger.d(TAG, "onGroupClick..groupPosition=" + groupPosition);
                TextView selectedTxtVw = (TextView) v.findViewById(R.id.lblListHeader);
                String selectedParentOption = selectedTxtVw.getText().toString();

                Intent intent = null;
                if (expListView.isGroupExpanded(groupPosition)) {
                    expListView.collapseGroupWithAnimation(groupPosition);
                } else {
                    expListView.expandGroupWithAnimation(groupPosition);
                }
                Map<String, Object> map1 = new HashMap<>();

                switch (selectedParentOption) {

                    //Home
                    case AppConstants.HOME:
//                        if (screenName.equalsIgnoreCase(LandingActivity.class.getSimpleName())) {
//                            closeDrawer();
//                            return false;
//                        }
//                        intent = new Intent(NavigationActivity.this, LandingActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        break;

                    //rediGO
                    case AppConstants.REDIGO:
//                        if (screenName.equalsIgnoreCase(FunPageDataActivity.class.getSimpleName())) {
//                            closeDrawer();
//                            return false;
//                        }
//                        intent = new Intent(NavigationActivity.this, FunPageDataActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        break;

                    //Book Now
                    case AppConstants.BOOK_NOW:
//                        if (screenName.equalsIgnoreCase(BookingActivity.class.getSimpleName())) {
//                            closeDrawer();
//                            return false;
//                        }
//                        intent = new Intent(NavigationActivity.this, BookingActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        break;

                    //Booking History
                    case AppConstants.BOOKING_HISTORY:
//                        if (screenName.equalsIgnoreCase(BookingHistoryActivity.class.getSimpleName())) {
//                            closeDrawer();
//                            return false;
//                        }
//                        intent = new Intent(NavigationActivity.this, BookingHistoryActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        break;

                    //Keep Me Posted or Submit Enquiry
                    case AppConstants.KEEP_ME_POSTED:
//                        if (screenName.equalsIgnoreCase(KeepMePostedDealerActivity.class.getSimpleName())) {
//                            closeDrawer();
//                            return false;
//                        }
//                        intent = new Intent(NavigationActivity.this, KeepMePostedConsumerActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        break;


                    //contact us
                    case AppConstants.CONTACT_US:
//                        if (screenName.equalsIgnoreCase(ContactUsActivity.class.getSimpleName())) {
//                            closeDrawer();
//                            return false;
//                        }
//                        intent = new Intent(NavigationActivity.this, ContactUsActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        break;
                }
                if (intent != null) {
                    startActivity(intent);
                    if (!(NavigationActivity.this instanceof MainActivity)) {
                        finish();
                    }
                }
                return true;
            }
        });

        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {


                                                @Override
                                                public boolean onChildClick(ExpandableListView parent, View v,
                                                                            int groupPosition, int childPosition, long id) {

                                                    TextView selectedTxtVw = (TextView) v.findViewById(R.id.lblListItem);
                                                    String selectedChildOption = selectedTxtVw.getText().toString();
                                                    if (selectedChildOption.startsWith("360")) {
                                                        selectedChildOption = "360";
                                                    }
                                                    Logger.d(TAG, "onChildClick.." + childPosition);
                                                    v.setSelected(true);
                                                    Intent intent = null;
                                                    Map<String, Object> map1 = new HashMap<>();

                                                    switch (selectedChildOption) {

                                                        //Highlights
                                                        case AppConstants.HIGHLIGHTS:

//                                                            if (screenName.equalsIgnoreCase(DesignPhilosphyActivity.class.getSimpleName())) {
//                                                                closeDrawer();
//                                                                return true;
//                                                            }
//                                                            intent = new Intent(NavigationActivity.this, DesignPhilosphyActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                            break;

                                                        //360
                                                        case AppConstants.VIEW_360:
//                                                            if (screenName.equalsIgnoreCase(Car360WebViewActivity.class.getSimpleName())) {
//                                                                closeDrawer();
//                                                                return true;
//                                                            }
//                                                            intent = new Intent(NavigationActivity.this, Car360WebViewActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                            break;


                                                        default:
                                                            break;
                                                    }

                                                    if (intent != null) {
                                                        startActivity(intent);
                                                        if (!(NavigationActivity.this instanceof MainActivity)) {
                                                            finish();
                                                        }

                                                    }

                                                    return true;
                                                }
                                            }

        );
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                Utils.animateActivity(this, "back");
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        navigationView = (NavigationView) findViewById(R.id.navigation_view);
      /*  int width = getResources().getDisplayMetrics().widthPixels / 2;
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            DrawerLayout.LayoutParams params = (DrawerLayout.LayoutParams) navigationView.getLayoutParams();
            params.width = width + (width / 8);
            navigationView.setLayoutParams(params);
        } else {
            DrawerLayout.LayoutParams params = (DrawerLayout.LayoutParams) navigationView.getLayoutParams();
            params.width = width + (width / 6);
            navigationView.setLayoutParams(params);
        }*/
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawers();
        } else {
            super.onBackPressed();
            Utils.animateActivity(NavigationActivity.this, "back");
        }
    }

    protected void setTitle(String title) {
        tvTitle.setVisibility(View.VISIBLE);
        tvTitle.setText(title);
    }

    protected void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    protected void closeDrawer() {
        drawerLayout.closeDrawers();

    }

    @Override
    protected void onStop() {
        super.onStop();
        closeDrawer();
    }

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {

    }

    @Override
    public void onDrawerOpened(View drawerView) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public void onDrawerClosed(View drawerView) {

    }

    @Override
    public void onDrawerStateChanged(int newState) {

    }

    private void prepareListData() {

        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();
        String[] array = getResources()
                .getStringArray(R.array.nav_drawer_items);
        listDataHeader = Arrays.asList(array);
        setConsumerData();
    }


    private void setConsumerData() {


        List<String> home_list = new ArrayList<String>();
        String[] dash = getResources().getStringArray(R.array.home_sublist);
        home_list = Arrays.asList(dash);

        List<String> about_us_list = new ArrayList<String>();
        String[] about_us = getResources()
                .getStringArray(R.array.about_us_sublist);
        about_us_list = Arrays.asList(about_us);


        List<String> vegetable_list = new ArrayList<String>();
        String[] vegetables = getResources()
                .getStringArray(R.array.vegetable_sublist);
        vegetable_list = Arrays.asList(vegetables);


        List<String> fruit_list = new ArrayList<String>();
        String[] fruits = getResources().getStringArray(R.array.fruits_sublist);
        fruit_list = Arrays.asList(fruits);


        List<String> privacy_policy_list = new ArrayList<String>();
        String[] privacy_policy = getResources().getStringArray(R.array.privacy_policy_sublist);
        privacy_policy_list = Arrays.asList(privacy_policy);

        List<String> term_condition_list = new ArrayList<String>();
        String[] term_condition = getResources().getStringArray(R.array.term_condition_sublist);
        term_condition_list = Arrays.asList(term_condition);

        List<String> contact_us_list = new ArrayList<String>();
        String[] contact_us = getResources().getStringArray(R.array.term_condition_sublist);
        contact_us_list = Arrays.asList(contact_us);


        listDataChild.put(listDataHeader.get(0), home_list);
        listDataChild.put(listDataHeader.get(1), about_us_list);
        listDataChild.put(listDataHeader.get(2), vegetable_list);
        listDataChild.put(listDataHeader.get(3), fruit_list);
        listDataChild.put(listDataHeader.get(4), privacy_policy_list);
        listDataChild.put(listDataHeader.get(5), term_condition_list);
        listDataChild.put(listDataHeader.get(6), contact_us_list);
    }

    public class ExpandableListAdapter extends AnimatedExpandableListView.AnimatedExpandableListAdapter {

        private Context _context;
        private List<String> _listDataHeader; // header titles
        // child data in format of header title, child title
        private HashMap<String, List<String>> _listDataChild;
        private int[] resources;

        public ExpandableListAdapter(Context context,
                                     List<String> listDataHeader,
                                     HashMap<String, List<String>> listChildData, int[] iconResources) {
            this._context = context;
            this._listDataHeader = listDataHeader;
            this._listDataChild = listChildData;
            this.resources = iconResources;
        }

        @Override
        public View getRealChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            final String childText = (String) getChild(groupPosition,
                    childPosition);

            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) this._context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.list_item, null);
            }

            TextView txtListChild = (TextView) convertView
                    .findViewById(R.id.lblListItem);

            txtListChild.setText(childText);
            return convertView;
        }

        @Override
        public int getRealChildrenCount(int groupPosition) {
            return _listDataChild.get(_listDataHeader.get(groupPosition)).size();
        }

        @Override
        public int getGroupCount() {
            return _listDataHeader.size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return _listDataHeader.get(groupPosition);
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return _listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition);
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            String headerTitle = (String) getGroup(groupPosition);
            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) this._context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.list_group, null);
            }
            // changing the +/- on expanded list view
            ImageView img_plusminus = (ImageView) convertView
                    .findViewById(R.id.plus_txt);
            if (isExpanded) {
                img_plusminus.setImageResource(R.drawable.ic_up);
            } else {
//                img_plusminus.setText("+");
                img_plusminus.setImageResource(R.drawable.ic_down);
            }

            List<String> childlist = listDataChild.get(listDataHeader.get(groupPosition));
            if (childlist.size() == 0)
                img_plusminus.setImageDrawable(null);

            TextView lblListHeader = (TextView) convertView
                    .findViewById(R.id.lblListHeader);
            // lblListHeader.setTypeface(null, Typeface.BOLD);
            lblListHeader.setText(headerTitle);

            ImageView imgListGroup = (ImageView) convertView
                    .findViewById(R.id.ic_txt);
            imgListGroup.setImageResource(resources[groupPosition]);

            return convertView;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

    }
}
