package com.app.android.vegetable.utils;


import com.app.android.vegetable.BuildConfig;

import java.util.HashMap;
import java.util.Map;


public class AppConstants {

    public static final String TAG = "Datsun";//BuildConfig.TAG;
    public static final String APP_SHARED_PREFERENCE = BuildConfig.APPLICATION_ID;
    public static final String LOGIN_OBJECT = "LOGIN_OBJECT";
    ;


    //hamberger
    public static final String HIGHLIGHTS = "Highlights";
    public static final String VIEW_360 = "360";


    //Parent
    public static final String HOME = "Home";
    public static final String REDIGO = "rediGO";
    public static final String BOOK_NOW = "Book Now";
    public static final String BOOKING_HISTORY = "Booking History";
    public static final String KEEP_ME_POSTED = "Keep Me Posted";
    public static final String CONTACT_US = "Contact Us";


    public static final String N_SEEN = "SEEN";

    public final static Map<String, String[]> PERMISSION_FIELDS_MAP = new HashMap<>();

    static {
        PERMISSION_FIELDS_MAP.put("email", new String[]{"email"});
        PERMISSION_FIELDS_MAP.put("user_about_me", new String[]{"gender", "first_name", "last_name"});
        PERMISSION_FIELDS_MAP.put("user_friends", new String[]{"friends"});
        PERMISSION_FIELDS_MAP.put("user_education_history", new String[]{"education"});
    }


}
