package com.app.android.vegetable.retrofit;




import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.Query;

/**
 * Created by Akash Raj Gupta on 4/22/2016.
 */
public interface RestInterface {


//    //http://www.cashbeez.com/appservice/web-member.php?api_key=[key]&param_query=Step_1&param_mobile_num=[value]&param_referral_id=[value]
//
//    @Headers("Cache-Control: no-cache")
//    @GET("/web-member.php")
//     void registerMobile(@Query("api_key") String api, @Query("param_query") String param, @Query("param_mobile_num") String mobile, @Query("param_referral_id") String referral, Callback<RegistrationApiModel> cb);
//
//
//    //http://www.cashbeez.com/appservice/web-member.php?api_key=[key]&amp;param_query=Step_2&amp;param_mobile_num=[value]&amp;param_otp=[value]
//
//    @GET("/web-member.php")
//    void otpVerification(@Query("api_key") String api, @Query("param_query") String param, @Query("param_mobile_num") String mobile, @Query("param_otp") String otp, Callback<OtpVerificationApiModel> cb);
//
//    //http://www.cashbeez.com/appservice/web-offer.php?api_key=85jgh8458543n45y835yhfgdjg&amp;param_query=GetOffer&amp;param_mobile_num=8826974666
//
//    @GET("/web-member.php")
//    void getOfferList(@Query("api_key") String api, @Query("param_query") String param, @Query("param_mobile_num") String mobile, Callback<OfferResponseApiModel> cb);
//
//    //http://www.cashbeez.com/appservice/web-member.php?api_key=85jgh8458543n45y835yhfgdjg&amp;param_query=Get_Web_Setting
//
//    @GET("/web-member.php")
//    void getWebSetting(@Query("api_key") String api, @Query("param_query") String param, Callback<WebSettingApiModel> cb);
//
//    //offer
//    //http://www.cashbeez.com/appservice/web-offer?api_key=85jgh8458543n45y835yhfgdjg&param_query=GetOffer&param_mobile_num=9236807123&param_search=
//
//    @GET("/web-offer")
//    void getOfferList(@Query("api_key") String api, @Query("param_query") String param, @Query("param_mobile_num") String mobile, @Query("param_search") String search, Callback<OfferResponseApiModel> cb);
//
//
//    //mywallet
//    //http://www.cashbeez.com/appservice/member-service.php?api_key=85jgh8458543n45y835yhfgdjg&param_query=MyWallet&param_mobile_num=8604071346
//
//    @GET("/member-service.php")
//    void getMyWalletData(@Query("api_key") String api, @Query("param_query") String param, @Query("param_mobile_num") String mobile, Callback<MyWalletApiModel> cb);
//
//
//    //profile update
//    //http://www.cashbeez.com/appservice/web-member.php?api_key=[key]&amp;param_query=M_Detail&amp;mobilenum=[value]&amp;name=[value]&amp;emailid=[value]&amp;dob=[value]&amp;gender=[value]&amp;city_id=[value]
//
//    @GET("/web-member.php")
//    void updateProfile(@Query("api_key") String api, @Query("param_query") String param, @Query("mobilenum") String mobile, @Query("name") String name, @Query("emailid") String emailId, @Query("dob") String dob, @Query("gender") String gender, @Query("city_id") String cityId, @Query("mtype") String mtype, @Query("mtype_id") String mtypeId, Callback<UpdateProfileApiModel> cb);
//
//    //update bank detail
//    //http://www.cashbeez.com/appservice/web-member.php?api_key=[key]&amp;param_query=B_Detail&amp;mobilenum=[value]&amp;ben_name=[value]&amp;bank_acc=[value]&amp;bank_name=[value]&amp;bank_branch=[value]&amp;branchcode=[value]&amp;ifsc=[value]&amp; pannum=[value]
//
//    @GET("/web-member.php")
//    void updateBankDetails(@Query("api_key") String api, @Query("param_query") String param, @Query("mobilenum") String mobile, @Query("ben_name") String benName, @Query("bank_acc") String bankAccount, @Query("bank_name") String bankName, @Query("bank_branch") String bankBranch, @Query("branchcode") String branchCode, @Query("ifsc") String ifsc, @Query("pannum") String pannum, Callback<UpdateBankDetailApiModel> cb);
//
//
//    //FAQ
//    //http://www.cashbeez.com/appservice/web-member.php?api_key=85jgh8458543n45y835yhfgdjg&param_query= Get_FAQ
//    @GET("/web-member.php")
//    void getFAQList(@Query("api_key") String api, @Query("param_query") String param, Callback<FAQApiModel> cb);
//
//    //MyActivity
//    //http://www.cashbeez.com/appservice/member-service.php?api_key=85jgh8458543n45y835yhfgdjg&param_query=MyActivity&param_mobile_num=3333333333&start_dt=&end_dt=2016-04-27
//    @GET("/member-service.php")
//    void getMyActivityList(@Query("api_key") String api, @Query("param_query") String param, @Query("param_mobile_num") String mobile, @Query("start_dt") String startDate, @Query("end_dt") String endDate, Callback<MyActivityApiModel> cb);
//
//
//    //MyTeam
//    //http://www.cashbeez.com/appservice/member-service.php?api_key=85jgh8458543n45y835yhfgdjg&param_query=MyTeam&param_mobile_num=3333333333
//    @GET("/member-service.php")
//    void getMyTeamMembers(@Query("api_key") String api, @Query("param_query") String param, @Query("param_mobile_num") String mobile, Callback<MyTeamApiModel> cb);
//
//
//    //MyReferral
//    //http://www.cashbeez.com/appservice/member-service.php?api_key=85jgh8458543n45y835yhfgdjg&param_query=MyReferral&param_mobile_num=8826974666
//    @GET("/member-service.php")
//    void getMyReferralList(@Query("api_key") String api, @Query("param_query") String param, @Query("param_mobile_num") String mobile, Callback<MyReferralApiModel> cb);
//
//    //MyPersonal Detail
//    //http://www.cashbeez.com/appservice/web-member.php?api_key=85jgh8458543n45y835yhfgdjg&param_query=Get_Member&mobilenum=9236807121
//
//    @GET("/web-member.php")
//    void getMyPersonalDetail(@Query("api_key") String api, @Query("param_query") String param, @Query("mobilenum") String mobile, Callback<PersonalDetailApiModel> cb);
//
//    //Fetch DTH Operator
//    //http://www.cashbeez.com/appservice/mob-recharge?api_key=[key]&amp;param_query=DTHOperator
//
//    @GET("/mob-recharge")
//    void fetchOperator(@Query("api_key") String api, @Query("param_query") String param, Callback<List<FetchOperatorApiModel>> cb);
//
//
//    //recharge Mobile/DTH
//    //http://www.cashbeez.com/appservice/mob-recharge?api_key=[key]&amp;param_query=MobileRecharge&amp;param_mobile_num=[value]&amp;provider_id=[value]&amp;amount=[value]
//    @GET("/mob-recharge")
//    void rechargeMobileOrDTHOperator(@Query("api_key") String api, @Query("param_query") String param, @Query("param_mobile_num") String mobile, @Query("provider_id") String providerId, @Query("amount") String amount, Callback<RechargeApiModel> cb);
//
//
//    //Get City List
//    //http://www.cashbeez.com/appservice/web-member.php?api_key=[key]&amp;param_query=Get_City_List
//    @GET("/web-member")
//    void getCityList(@Query("api_key") String api, @Query("param_query") String param, Callback<CityApiModel> cb);
//
//    //Bank Transfer
//    //http://www.cashbeez.com/appservice/member-service?api_key=85jgh8458543n45y835yhfgdjg&param_query=PaymentRequest&param_mobile_num=8826974666&amount=10&remark=%22aKASH%20rAJ%20gUPTA%22
//    @GET("/member-service")
//    void bankTransferPayment(@Query("api_key") String api, @Query("param_query") String param, @Query("param_mobile_num") String mobile, @Query("amount") String amount, @Query("remark") String remark, Callback<BankTransferApiModel> cb);
//
//    //GetTransactionHostory
//    //http://www.cashbeez.com/appservice/member-service?param_query=MyTranHistory&amp;param_mobile_num=8826974666
//    @GET("/member-service")
//    void transactionHistory(@Query("api_key") String api, @Query("param_query") String param, @Query("param_mobile_num") String mobile, Callback<TransactionApiModel> cb);
}
