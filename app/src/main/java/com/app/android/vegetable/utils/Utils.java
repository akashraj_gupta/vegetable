package com.app.android.vegetable.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Patterns;
import android.widget.Toast;


import com.app.android.vegetable.ApplicationController;
import com.app.android.vegetable.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import retrofit.RetrofitError;


/**
 * Created by ashish123 on 27/6/15.
 */
public class Utils {

    private static final String TAG = "Utils.java";

    public static boolean checkInternetConnection(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null) {
            return false;
        } else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public final static boolean isValidEmail(String target) {
        if (target == null) {
            return false;
        } else {
            return Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }


    public final static boolean isValidPhoneNumber(String phoneNumber) {
        if (!TextUtils.isEmpty(phoneNumber)) {
            return Patterns.PHONE.matcher(phoneNumber).matches();
        }
        return false;
    }

    public static int getDpToPx(Context context, int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }


    public static float convertPixelsToDp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        //  Logger.e(TAG, "density dpi: " + metrics.densityDpi);
        return dp;
    }

    public static DisplayMetrics getDisplayMetrics() {
        return ApplicationController.getInstance().getResources().getDisplayMetrics();
    }


    public static void animateActivity(Activity activity, String action) {


        if (action.equalsIgnoreCase("next")) {

            activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        } else if (action.equalsIgnoreCase("back")) {
            activity.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);

        } else if (action.equalsIgnoreCase("up")) {
            activity.overridePendingTransition(R.anim.push_up_in,
                    R.anim.push_up_out);
        } else if (action.equalsIgnoreCase("down")) {
            activity.overridePendingTransition(R.anim.push_down_in,
                    R.anim.push_down_out);
        } else if (action.equalsIgnoreCase("fadein")) {
            activity.overridePendingTransition(R.anim.fade_in,
                    R.anim.fade_out);
        } else if (action.equalsIgnoreCase("zero")) {
            activity.overridePendingTransition(R.anim.zero_duration,
                    R.anim.zero_duration);
        }

    }






    public static String getNetworkClass(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info == null || !info.isConnected())
            return "-"; //not connected
        if (info.getType() == ConnectivityManager.TYPE_WIFI)
            return "WIFI";
        if (info.getType() == ConnectivityManager.TYPE_MOBILE) {
            int networkType = info.getSubtype();
            switch (networkType) {
                case TelephonyManager.NETWORK_TYPE_GPRS:
                case TelephonyManager.NETWORK_TYPE_EDGE:
                case TelephonyManager.NETWORK_TYPE_CDMA:
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                case TelephonyManager.NETWORK_TYPE_IDEN: //api<8 : replace by 11
                    return "2G";
                case TelephonyManager.NETWORK_TYPE_UMTS:
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                case TelephonyManager.NETWORK_TYPE_HSPA:
                case TelephonyManager.NETWORK_TYPE_EVDO_B: //api<9 : replace by 14
                case TelephonyManager.NETWORK_TYPE_EHRPD:  //api<11 : replace by 12
                case TelephonyManager.NETWORK_TYPE_HSPAP:  //api<13 : replace by 15
                    return "3G";
                case TelephonyManager.NETWORK_TYPE_LTE:    //api<11 : replace by 13
                    return "4G";
                default:
                    return "?";
            }
        }
        return "?";
    }

    public static String getReplacementText(int stringResource, String... args) {
        return ApplicationController.getInstance().getResources().getString(stringResource, args);
    }

    public static void openAppSettings(Context context) {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", context.getPackageName(), null);
        intent.setData(uri);
        context.startActivity(intent);
    }

    public static void handleErrorDialog(RetrofitError error, Activity activity) {
        switch (error.getKind()) {
            case NETWORK:
                break;

            case UNEXPECTED:
                break;

            case CONVERSION:
                break;

            case HTTP:
                break;
        }
    }

    public static void handleErrorToast(RetrofitError error) {
        switch (error.getKind()) {
            case NETWORK:
                Toast.makeText(ApplicationController.getInstance(),
                        ApplicationController.getInstance().getString(R.string.network_error),
                        Toast.LENGTH_SHORT).show();
                break;

            case UNEXPECTED:

                Toast.makeText(ApplicationController.getInstance(),
                        ApplicationController.getInstance().getString(R.string.server_error),
                        Toast.LENGTH_SHORT).show();
                break;

            case CONVERSION:
                Toast.makeText(ApplicationController.getInstance(),
                        ApplicationController.getInstance().getString(R.string.application_error),
                        Toast.LENGTH_SHORT).show();
                break;

            case HTTP:
                Toast.makeText(ApplicationController.getInstance(),
                        ApplicationController.getInstance().getString(R.string.server_error),
                        Toast.LENGTH_SHORT).show();
                break;
        }
    }

//    @Nullable
//    public static Cursor getDealerCursor(Context context, BasicListItemModel selectedCity) {
//        if (selectedCity == null) {
//            return null;
//        } else {
//            String selection = DealertableColumns.CITY_ID + " = ?";
//            String[] selectionArgs = new String[]{
//                    selectedCity.getId()
//            };
//
//            return context.getContentResolver().query(DealertableColumns.CONTENT_URI, DealertableColumns.ALL_COLUMNS, selection, selectionArgs, null);
//        }
//
//    }

//    public static Class getNotificationActivityClass(String tag) {
//        //decide on based of tag what to return
//
//        UserProfileModel userProfileModel = SharedPreference.getUserData(ApplicationController.getInstance());
//        if (userProfileModel == null || !userProfileModel.isUserLoggedIn()) {
//            return SplashActivity.class;
//        }
//        else
//            return KnowTheCarActivity.class;
//    }

    public static int getNavigationBarHeight(Context context) {

        int orientation = context.getResources().getConfiguration().orientation;

        Resources resources = context.getResources();

        int id = resources.getIdentifier(
                orientation == Configuration.ORIENTATION_PORTRAIT ? "navigation_bar_height" : "navigation_bar_height_landscape",
                "dimen", "android");
        if (id > 0) {
            return resources.getDimensionPixelSize(id);
        } else
            return Utils.getDpToPx(context, 42);
    }




    public static final String getImageNameFromUrl(String s) {

        return s.substring(s.lastIndexOf("/") + 1);

    }

    public static File returnFileIfExist(Context context, String imageUrl) {

        String imageName = getImageNameFromUrl(imageUrl);
        File file = new File(context.getExternalFilesDir(null), imageName + ".jpg");
        if (file.exists()) return file;
        else return null;
    }



    public static void navigateFragment(Fragment fragment, String tag, Bundle bundle, FragmentActivity fragmentActivity) {
        FragmentManager fragmentManager = fragmentActivity.getSupportFragmentManager();
        fragmentManager.popBackStackImmediate();
        boolean fragmentPopped = fragmentManager.popBackStackImmediate(tag, 0);
    /* *//*
         * fragment not in back stack, create it.
*//* */
        if (!fragmentPopped && fragmentManager.findFragmentByTag(tag) == null) {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            if (bundle != null) {
                fragment.setArguments(bundle);
            }
            fragmentTransaction.replace(R.id.container, fragment, tag);
            fragmentTransaction.addToBackStack(tag);
            fragmentManager.popBackStack();
            fragmentTransaction.commit();
        }
    }
}