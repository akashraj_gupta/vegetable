package com.app.android.vegetable;

import android.app.Application;

/**
 * Created by atarmanipandey on 29/3/16.
 */

public class ApplicationController extends Application {
    private static final String TAG = "ApplicationController";
    private static ApplicationController mInstance;




    public static ApplicationController getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

    }





}
