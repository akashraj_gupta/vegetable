package com.app.android.vegetable.retrofit;

import com.squareup.okhttp.ConnectionPool;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;

/**
 * Created by Akash Raj Gupta on 4/22/2016.
 */
public class RestClient {

    private static RestInterface REST_CLIENT;
    private static String ROOT =
            "http://www.cashbeez.com/appservice";
    private static long SIZE_OF_CACHE = 10 * 1024 * 1024; // 10 MB

    static {
        setupRestClient();
    }

    private RestClient() {}

    public static RestInterface get() {
        return REST_CLIENT;
    }


    private static final Interceptor REWRITE_CACHE_CONTROL_INTERCEPTOR = new Interceptor() {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Response originalResponse = chain.proceed(chain.request());
            return originalResponse.newBuilder()
                    .header("Cache-Control", String.format("max-age=%d, only-if-cached, max-stale=%d", 120, 0))
                    .build();
        }
    };

    private static void setupRestClient() {
        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setConnectTimeout(90, TimeUnit.SECONDS);
        okHttpClient.setWriteTimeout(90, TimeUnit.SECONDS);
        okHttpClient.setReadTimeout(90, TimeUnit.SECONDS);
        okHttpClient.setRetryOnConnectionFailure(true);
        okHttpClient.setConnectionPool(new ConnectionPool(4, 120000));


        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(ROOT)
                .setClient(new CustomOKHttpClient(okHttpClient))
                .setLogLevel(RestAdapter.LogLevel.FULL);

        RestAdapter restAdapter = builder.build();
        REST_CLIENT = restAdapter.create(RestInterface.class);
    }
}
